
#include <SPI.h>
#include <Ethernet.h>
#include <Wire.h>

byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
const byte interruptPin1 = 32;
const byte interruptPin2 = 33;
const byte interruptPin3 = 34;
const byte interruptPin4 = 35;
volatile long count1 = 0;
volatile long waktu1 = 0;
volatile long count2 = 0;
volatile long waktu2 = 0;
volatile long count3 = 0;
volatile long waktu3 = 0;
volatile long count4 = 0;
volatile long waktu4 = 0;
long data1, data2, data3, data4;

//char server[] = "greenhouseku.xyz";    // name address for Google (using DNS)
//IPAddress ip(192, 168, 0, 177);

IPAddress server(192, 168, 0, 212);
IPAddress ip(192, 168, 0, 122);
IPAddress myDns(202, 46, 129, 2);
EthernetClient client;

//unsigned long beginMicros, endMicros;
//unsigned long byteCount = 0;
//bool printWebData = true;  // set to false for better speed measurement
//String alamat = "GET /inputdata1.php?data1=" + String(data1) + "&data2=" + String(data2) + "&data3=" + String(data3) + "&data4=" + String(data4);

void setup() {

  pinMode(interruptPin1, INPUT_PULLUP);
  pinMode(interruptPin2, INPUT_PULLUP);
  pinMode(interruptPin3, INPUT_PULLUP);
  pinMode(interruptPin4, INPUT_PULLUP);

  Ethernet.init(5);  // ESP32 with Adafruit Featherwing Ethernet

  // Open serial communications and wait for port to open:
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }
  Ethernet.begin(mac, ip, myDns);
  // give the Ethernet shield a second to initialize:
  delay(1000);
  //Serial.println("connecting to ");
  Serial.print(server);
  Serial.println("...");
}

void loop() {
  attachInterrupt(digitalPinToInterrupt(interruptPin1), blink1, FALLING);
  attachInterrupt(digitalPinToInterrupt(interruptPin2), blink2, FALLING);
  attachInterrupt(digitalPinToInterrupt(interruptPin3), blink3, FALLING);
  attachInterrupt(digitalPinToInterrupt(interruptPin4), blink4, FALLING);
  if (data1 == 0) {
    data1 = 1;
  }
//  if (data2 == 0) {
//    data2 = 1;
//  }
//  if (data3 == 0) {
//    data3 = 1;
//  }
//  if (data4 == 0) {
//    data4 = 1;
//  }
  String p = ",";
  //Serial.println(data1 + p + data2 + p + data3 + p + data4);
  Serial.println(data1);
  //client.println ("greenhouseku.xyz/inputdata1.php?data1=1&data2=12&data3=12&data4=19");
  delay (1000);
  //httpRequest();
  
//    if (client.connect(server, 80)) {
//      Serial.println(server);
//      //Serial.println(client.remoteIP());
//      // Make a HTTP request:
//      client.println ( "GET /inputdata2.php?data1=" + String(data1) + "&data2=" + String(data2) + "&data3=" + String(data3) + "&data4=" + String(data4));
//     // client.println ( "GET /inputdata1.php?data1=" + String(data1) + "&data2=" + String(data2) + "&data3=" + String(data3) + "&data4=" + String(data4));
//      // client.println("Host: www.google.com");
//      client.println("Connection: close");
//      client.println();
//    }
//    else {
//      Serial.println("connection failed");
//    }
}
void blink1() {
  if (count1 == 0) {
    waktu1 = micros();
    count1++;
  }
  else if (count1 > 99) {
    long periode1 = (micros() - waktu1) / 100;
    data1 = periode1;
    //    Serial.print(periode);
    //    Serial.println("");
    count1 = 0;
    detachInterrupt(digitalPinToInterrupt(interruptPin1));
  }
  else count1++;
}
void blink2() {
  if (count2 == 0) {
    waktu2 = micros();
    count2++;
  }
  else if (count2 > 99) {
    long periode2 = (micros() - waktu2) / 100;
    data2 = periode2;
    //    Serial.print(periode);
    //    Serial.println("");
    count2 = 0;
    detachInterrupt(digitalPinToInterrupt(interruptPin2));
  }
  else count2++;
}
void blink3() {
  if (count3 == 0) {
    waktu3 = micros();
    count3++;
  }
  else if (count3 > 99) {
    long periode3 = (micros() - waktu3) / 100;
    data3 = periode3;
    //    Serial.print(periode);
    //    Serial.println("");
    count3 = 0;
    detachInterrupt(digitalPinToInterrupt(interruptPin3));
  }
  else count3++;
}
void blink4() {
  if (count4 == 0) {
    waktu4 = micros();
    count4++;
  }
  else if (count4 > 99) {
    long periode4 = (micros() - waktu4) / 100;
    data4 = periode4;
    //    Serial.print(periode);
    //    Serial.println("");
    count4 = 0;
    detachInterrupt(digitalPinToInterrupt(interruptPin4));
  }
  else count4++;
}
