#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>

#define SEALVLPRS_HPA (1013.25)

Adafruit_BME280 bme;

const byte interruptPin1 = 2;
const byte interruptPin2 = 15;
const byte interruptPin3 = 5;
const byte interruptPin4 = 18;
volatile long count1 = 0;
volatile long waktu1 = 0;
volatile long count2 = 0;
volatile long waktu2 = 0;
volatile long count3 = 0;
volatile long waktu3 = 0;
volatile long count4 = 0;
volatile long waktu4 = 0;
int temp, hum;
long data1,data2,data3,data4;
char buf[20];

void setup() {
  Serial.begin(9600);
  pinMode(interruptPin1, INPUT_PULLUP);
  pinMode(interruptPin2, INPUT_PULLUP);
  pinMode(interruptPin3, INPUT_PULLUP);
  pinMode(interruptPin4, INPUT_PULLUP);

  bme.begin (0x76);

}

void loop() {
  attachInterrupt(digitalPinToInterrupt(interruptPin1), blink1, FALLING);
  attachInterrupt(digitalPinToInterrupt(interruptPin2), blink2, FALLING);
  attachInterrupt(digitalPinToInterrupt(interruptPin3), blink3, FALLING);
  attachInterrupt(digitalPinToInterrupt(interruptPin4), blink4, FALLING);
  Bme();
    if (data1 == 0) {
    data1 = 1;
  }
  if (data2 == 0) {
    data2 = 1;
  }
  if (data3 == 0) {
    data3 = 1;
  }
  if (data4 == 0) {
    data4 = 1;
  }
  
   Serial.print(temp);
  Serial.print(":");
  Serial.print(hum);
  Serial.print(":");
  String p = ","; 
  Serial.println(data1 + p + data2 + p + data3+ p + data4);
  delay (1000);
  
}

void blink1() {
  if(count1==0){
    waktu1 =micros();
    count1++;
  }
  else if (count1>99){
    long periode1=(micros()-waktu1)/100;
    data1=periode1;
//    Serial.print(periode);
//    Serial.println("");
    count1=0; 
    detachInterrupt(digitalPinToInterrupt(interruptPin1));
  }
  else count1++;
}
void blink2(){
  if(count2==0){
    waktu2 =micros();
    count2++;
  }
  else if (count2>99){
    long periode2=(micros()-waktu2)/100;
    data2=periode2;
//    Serial.print(periode);
//    Serial.println("");
    count2=0; 
    detachInterrupt(digitalPinToInterrupt(interruptPin2));
  }
  else count2++;
}
void blink3(){
  if(count3==0){
    waktu3 =micros();
    count3++;
  }
  else if (count3>99){
    long periode3=(micros()-waktu3)/100;
    data3=periode3;
//    Serial.print(periode);
//    Serial.println("");
    count3=0; 
    detachInterrupt(digitalPinToInterrupt(interruptPin3));
  }
  else count3++;
}
void blink4() {
  if(count4==0){
    waktu4 =micros();
    count4++;
  }
  else if (count4>99){
    long periode4=(micros()-waktu4)/100;
    data4=periode4;
//    Serial.print(periode);
//    Serial.println("");
    count4=0; 
    detachInterrupt(digitalPinToInterrupt(interruptPin4));
  }
  else count4++;
}
void Bme(){
  temp = bme.readTemperature();
  hum = bme.readHumidity();
 
  
  
  }
