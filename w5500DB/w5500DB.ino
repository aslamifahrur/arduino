
#include <SPI.h>
#include <Ethernet.h>
 
byte mac[] = {
  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED
};
IPAddress ip(192, 168, 1, 134); //Access Point-1 192.168.1.151; Access Point-2 192.168.1.152

// initialize the library instance:
EthernetClient client;
IPAddress server(192, 168, 1, 109); 

/*anemometer*/
int sensoranemo = 0;
float voltage = 0.00;
int wind;

/*winddirection*/
int sensorwind = 0;
unsigned int derajat1;

void setup() {
  // start serial port:
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }

  delay(1000);
  // start the Ethernet connection using a fixed IP address and DNS server:
  Ethernet.begin(mac, ip);
  // print the Ethernet board/shield's IP address:
  Serial.print("My IP address: ");
  Serial.println(Ethernet.localIP());
}

void loop() {
//  Serial.println("1");
  bacaanemo();
//  Serial.println("2");
  bacaarah();
//  Serial.println("3");
  httpRequest_anemometer();
//  Serial.println("4");
  httpRequest_winddirection();
//  Serial.println("5");
  delay(500);
}

void bacaanemo() {
  sensoranemo = analogRead(A1);
  voltage = sensoranemo * (5.0 / 1023.0);

  if (voltage <= 3.2) {
    wind = voltage * (19.2 / 3.2)*100;
  } else {
    wind = voltage * (30 / 5)*100; 
  }
  Serial.println(wind);
}
void httpRequest_anemometer() {
  client.stop();

  //pastikan server = (server, 8080) begitu juga dengan yang di xampp
  if (client.connect(server, 80)) {
    Serial.print("connecting...   ");
    // send the HTTP GET request:
    Serial.print("kecepatan:  ");
    Serial.print(wind);
    Serial.println("  m/s");
//    Serial.println(data2);
    client.println("GET /waterlevel/inputdata1.php?data1="+String(wind));
//    +"&data2="+String(data2));

    client.println("Host: 192, 168, 1, 23:80");
    client.println("Connection: close");
    client.println();
  } else {
    Serial.println("connection failed");
  }
}
void bacaarah() {
  sensorwind = analogRead(A0);
  derajat1 = sensorwind * (360 / 1036.0);
}

void httpRequest_winddirection() {
  client.stop();

  //pastikan server = (server, 8080) begitu juga dengan yang di xampp
  if (client.connect(server, 80)) {
    Serial.print("connecting...   ");
    // send the HTTP GET request:
    Serial.print("arah:  ");
    Serial.println(derajat1);
//    Serial.println(data2);
    client.println("GET /waterlevel/inputdata2.php?data1="+String(derajat1));
//    +"&data2="+String(data2));

    client.println("Host: 192, 168, 1, 23:80");
    client.println("Connection: close");
    client.println();
  } else {
    Serial.println("connection failed");
  }
}
