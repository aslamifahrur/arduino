

const byte interruptPin = 2;
volatile long count = 0;
volatile long waktu = 0;

void setup() {
  Serial.begin(9600);
  pinMode(interruptPin, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(interruptPin), blink, FALLING);
}

void loop() {
  
}

void blink() {
  if(count==0){
    waktu =micros();
    count++;
  }
  else if (count>99){
    long periode=(micros()-waktu)/100;
    Serial.println(periode);
    count=0; 
  }
  else count++;
}
